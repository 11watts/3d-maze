# 3D Maze#

A simple 3D first-person maze created with three.js

![3d.png](https://bitbucket.org/repo/GparXE/images/3626032229-3d.png)

You can try it [here](http://11watts.bitbucket.org/3D-maze/)