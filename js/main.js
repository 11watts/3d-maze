var scene;
var camera;
var rendere;
var controls;
var clock;
var raycaster;
var objects = [];
var playerRadius = 6;
var rays = [
      new THREE.Vector3(0, 0, 1),
      new THREE.Vector3(1, 0, 0),
      new THREE.Vector3(0, 0, -1),
      new THREE.Vector3(-1, 0, 1)
    ];
window.onload = function(){
	var blocker = document.getElementById( 'blocker' );
	var instructions = document.getElementById( 'instructions' );


	var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
	console.log(havePointerLock);
	if(havePointerLock){
		var element = document.body;
		var pointerlockchange = function(event){
			if (document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element){
				controls.enabled = true;
				blocker.style.display = 'none';
			}
			else{
				controls.enabled = false;
				blocker.style.display = '-webkit-box';
				blocker.style.display = '-moz-box';
				blocker.style.display = 'box';
				instructions.style.display = '';
			}
		}
		var pointerlockerror = function(event){
			console.log('pointerlockerror');
		}
	}
	document.addEventListener( 'pointerlockchange', pointerlockchange, false );
	document.addEventListener( 'mozpointerlockchange', pointerlockchange, false );
	document.addEventListener( 'webkitpointerlockchange', pointerlockchange, false );
	document.addEventListener( 'pointerlockerror', pointerlockerror, false );
	document.addEventListener( 'mozpointerlockerror', pointerlockerror, false );
	document.addEventListener( 'webkitpointerlockerror', pointerlockerror, false );


	instructions.addEventListener('click', function(event){
		instructions.style.display = 'none';
		element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
			if (/Firefox/i.test( navigator.userAgent )){
				var fullscreenchange = function(event){
					if(document.fullscreenElement === element || document.mozFullscreenElement === element || document.mozFullScreenElement === element){
						document.removeEventListener( 'fullscreenchange', fullscreenchange );
						document.removeEventListener( 'mozfullscreenchange', fullscreenchange );
						element.requestPointerLock();
						}
					}
					document.addEventListener('fullscreenchange', fullscreenchange, false);
					document.addEventListener('mozfullscreenchange', fullscreenchange, false);
					element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;
					element.requestFullscreen();
				}
				else{
						element.requestPointerLock();
				}
			}, false);


	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000);
	renderer = new THREE.WebGLRenderer();
	clock = new THREE.Clock();


	renderer.setClearColor(new THREE.Color(0x1b85b8, 1.0));
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);



	controls = new THREE.PointerLockControls(camera);
	scene.add(controls.getObject());
	raycaster = new THREE.Raycaster();

	var w = 12;
	var h = 20;

	controls.getObject().position.set(w * 40 - 20, 0, 20);

	var planeGeometry = new THREE.PlaneGeometry(w * 40 + 800, h * 40 + 800);
	var planeMaterial = new THREE.MeshLambertMaterial({color: 0x559e83});
	var plane = new THREE.Mesh(planeGeometry,planeMaterial);

	plane.position.x = w * 20;
	plane.position.z = h * 20;
	plane.rotation.x = -.5 * Math.PI;
	scene.add(plane);


	var hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 1);
  hemiLight.position.set(0, 100, 0);
  hemiLight.visible = true;
  scene.add(hemiLight);

	var flashlight = new THREE.SpotLight(0xffffff, 1, 800, Math.PI/3);
	camera.add(flashlight);
	flashlight.position.set(0, 1, 1);
	flashlight.target = camera;

	// Gen maze
	var maze = new recursiveBacktracker(w, h);
	var walls = maze.allSteps();

	var cube;
	var cubeGeometry1 = new THREE.BoxGeometry(40,20,4);
	var cubeGeometry2 = new THREE.BoxGeometry(4,20,40);
	var cubeMaterial = new THREE.MeshLambertMaterial({color: 0x5a5255});


	for(var i = 0; i < w; i++){
		for(var j = 0; j < h; j++){
			if(walls[i][j][0] === true){
				cube = new THREE.Mesh(cubeGeometry1, cubeMaterial);
				cube.position.x = i * 40 + 20;
				cube.position.z = j * 40;
				cube.position.y = 10;
				cube.castShadow = true;
				scene.add(cube);
				objects.push(cube);
			}
			if(walls[i][j][1] === true){
				cube = new THREE.Mesh(cubeGeometry2, cubeMaterial);
				cube.position.x = i * 40 + 40;
				cube.position.z = j * 40 + 20;
				cube.position.y = 10;
				cube.castShadow = true;
				scene.add(cube);
				objects.push(cube);
			}
		}
	}
	var missingWall = Math.floor(Math.random() * (w + h));
	for(var i = 0; i < w; i++){
		if(i !== missingWall){
			cube = new THREE.Mesh(cubeGeometry1, cubeMaterial);
			cube.position.x = i * 40 + 20;
			cube.position.z = h * 40;
			cube.position.y = 10;
			cube.castShadow = true;
			scene.add(cube);
			objects.push(cube);
		}
	}
	for(var j = 0; j < h; j++){
		if((j + w) !== missingWall){
			cube = new THREE.Mesh(cubeGeometry2, cubeMaterial);
			cube.position.x = 0;
			cube.position.z = j * 40 + 20;
			cube.position.y = 10;
			cube.castShadow = true;
			scene.add(cube);
			objects.push(cube);
		}
	}
	animate();
}

function animate() {

	requestAnimationFrame(animate);
	raycaster.ray.origin.copy(controls.getObject().position);
	for (i = 0; i < rays.length; i += 1){
		raycaster.set(controls.getObject().position, rays[i]);
		var intersections = raycaster.intersectObjects(objects);
		if (intersections.length > 0 && intersections[0].distance < playerRadius){
			if(i === 0){
				controls.getObject().position.z -= (playerRadius - intersections[0].distance);
			}
			if(i === 1){
				controls.getObject().position.x -= (playerRadius - intersections[0].distance);
			}
			if(i === 2){
				controls.getObject().position.z += (playerRadius - intersections[0].distance);
			}
			if(i === 3){
				controls.getObject().position.x += (playerRadius - intersections[0].distance);
			}
		}
	}
	controls.update();
	renderer.render(scene, camera);
}

function recursiveBacktracker(w, h){
	var cellsLeft = w * h;
	var maze = {};
	maze.walls = [];
	maze.cells = [];
	var stack = [];

	for(var i = 0; i < w; i++){
		maze.walls.push([]);
		maze.cells.push([]);
		for(var j = 0; j < h; j++){
			maze.walls[i].push([true, true, true, true]);
			maze.cells[i].push(0);
		}
	}

	var unvisited = function(d, x, y){
		//0 is N, 1 is E, 2 is S, 3 is W
		var off = {x: 0, y: 0};

		switch(d){
			case 0:
				off.y = -1;
				break;
			case 1:
				off.x = 1;
				break;
			case 2:
				off.y = 1;
				break;
			case 3:
				off.x = -1;
				break;
		}

		if(maze.walls[x + off.x]){

			if(maze.cells[x + off.x][y + off.y] === 0){
				return true;
			}
		}
		return false;

	};

	var step = function(){

		if(stack.length === 0){
			var i = Math.floor(Math.random() * w);
			var j = Math.floor(Math.random() * h);
			stack = [{x: i, y: j}];
			maze.cells[i][j] = 1;
			cellsLeft--;
		}

		var curr = stack[stack.length - 1];
		var unvisitedCells = [];

		maze.walls[curr.x][curr.y].forEach(function(e, index){

			if(e && unvisited(index, curr.x, curr.y)){
				unvisitedCells.push(index);
			}

			if(index === 3){

				if(unvisitedCells.length === 0){
					// Mark as visited
					maze.cells[curr.x][curr.y] = 1;
					stack.pop();
				}
				// Pick random cell
				else{
					var d = unvisitedCells[Math.floor(Math.random() * unvisitedCells.length)];
					var off = {x: 0, y: 0};
					// Remove wall
					switch(d){
						case 0:
							off.y = -1;
							maze.walls[curr.x][curr.y][0] = false;
							maze.walls[curr.x][curr.y - 1][2] = false;
							break;
						case 1:
							off.x = 1;
							maze.walls[curr.x][curr.y][1] = false;
							maze.walls[curr.x + 1][curr.y][3] = false;
							break;
						case 2:
							off.y = 1;
							maze.walls[curr.x][curr.y][2] = false;
							maze.walls[curr.x][curr.y + 1][0] = false;
							break;
						case 3:
							off.x = -1;
							maze.walls[curr.x][curr.y][3] = false;
							maze.walls[curr.x - 1][curr.y][1] = false;
							break;
					}
					// Mark as visited
					maze.cells[curr.x][curr.y] = 1;
					// Add to top of stack
					stack.push({x: curr.x + off.x, y: curr.y + off.y});
					cellsLeft--;
				}
			}
		});
	};

	var self = this;
	this.stepsSlow = function(waitTime){
		var l = stack.length;
		step();
		drawMaze(maze.walls);
		while(l > stack.length){
			l = stack.length;
			step();
		}
		if(cellsLeft > 0){
			setTimeout(self.stepsSlow, waitTime, waitTime);
		}
	};

	this.allSteps = function(){
		while(cellsLeft > 0){
			step();
		}
		return maze.walls;
	}
	this.stop
}
